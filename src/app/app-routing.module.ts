import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table/table.component';
import { SchemeComponent } from './scheme/scheme.component';
import { NormsComponent } from './norms/norms.component';

const routes: Routes = [
  { path: '', component: TableComponent },
  { path: 'table-component', component: TableComponent },
  { path: 'scheme-component', component: SchemeComponent },
  { path: 'norms-component', component: NormsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
