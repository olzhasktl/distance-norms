import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
// import * as fs from 'fs';
import { IEvent } from 'angular8-yandex-maps';

class Point {
  constructor(
    public id: number,
    public lat: number,
    public long: number,
    public radius: number
  ) {}

  public get feature() {
    return {
      geometry: {
        type: "Circle",
        coordinates: [this.lat, this.long],
        radius: this.radius
      }
    }
  }

  public get options() {
    return {
      fillColor: '#78866b',
      strokeColor: '#50861d',
      opacity: 0.2,
      strokeWidth: 1,
      // strokeStyle: 'shortdash',
      draggable: true
    }
  }
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  public points: Point[] = []
  userTable: FormGroup;
  control: FormArray;
  touchedRows: any;
  currentGroup = null;
  coef = 4.0;
  max_radius = 100;
  radius_coeff = 1000;
  colors = {
    'green': 'подземный',
    'gray': 'ангар',
    'brown': 'хранилище',
  };
  rows = [ { "object_number": 1, "vv_amount": 10, "vagon_amount": 150, "group_type": "Б", "group_name": 4, "coord_x": 46.869482321228524, "coord_y": 75.34366449788314, "radius": 8.611126938897492, "isEditable": false, "color": "gray" }, { "object_number": 3, "vv_amount": 49, "vagon_amount": 200, "group_type": "Г", "group_name": 2, "coord_x": 46.87795807382548, "coord_y": 75.37181696370344, "radius": 14.618246671074115, "isEditable": false, "color": "green" }, { "object_number": 434, "vv_amount": 432, "vagon_amount": 432, "group_type": "Б", "group_name": "1", "coord_x": 46.86995323171171, "coord_y": 75.40408930257065, "radius": 30.177001122127802, "color": "gray", "isEditable": false }, { "object_number": 13, "vv_amount": 123, "vagon_amount": 123, "group_type": "В", "group_name": "1", "coord_x": 46.844989246868096, "coord_y": 75.34366449788314, "radius": 19.860875703145698, "color": "brown", "isEditable": false }, { "object_number": 10, "vv_amount": 140, "vagon_amount": 150, "group_type": "A", "group_name": "3", "coord_x": 46.824962520481556, "coord_y": 75.38932642415146, "radius": 20.735791970833695, "color": "green", "isEditable": false }, { "object_number": 4, "vv_amount": 50, "vagon_amount": 3, "group_type": "В", "group_name": "2", "coord_x": 46.906012622416775, "coord_y": 75.37113031835855, "radius": 14.716922496736416, "color": "gray", "isEditable": false } ];
  rules = {
    "A": {
      "1": { max_vv: 150, max_vagon: 15},
      "2": { max_vv: 100, max_vagon: 10},
      "3": { max_vv: 120, max_vagon: 30},
      "4": { max_vv: 200, max_vagon: 50},
      "5": { max_vv: 120, max_vagon: 40},
    },
    "Б": {
      "1": { max_vv: 50, max_vagon: 50}
    },
    "В": {
      "1": { max_vv: 150, max_vagon: 50},
      "2": { max_vv: 120, max_vagon: 50}
    },
    "Г": {
      "1": { max_vv: 150, max_vagon: 50},
      "2": { max_vv: 150, max_vagon: 50},
      "3": { max_vv: 150, max_vagon: 50},
      "4": { max_vv: 150, max_vagon: 50},
      "5": { max_vv: 0, max_vagon: 25}
    },
    "Д": {
      "1": { max_vv: 150, max_vagon: 50},
      "2": { max_vv: 150, max_vagon: 50},
      "3": { max_vv: 150, max_vagon: 50},
      "4": { max_vv: 150, max_vagon: 50},
      "5": { max_vv: 150, max_vagon: 50},
      "6": { max_vv: 150, max_vagon: 50},
      "7": { max_vv: 150, max_vagon: 50},
      "8": { max_vv: 150, max_vagon: 50},
      "9": { max_vv: 150, max_vagon: 50}
    }
  };
  group_names = {};
  constructor(private fb: FormBuilder, private _changeDetectorRef: ChangeDetectorRef) {
    for (let i of this.rows) {
      this.points.push(new Point(i['object_number'], i['coord_x'], i['coord_y'], i['radius']*this.radius_coeff));
    }
  }
  public feature = {
    geometry: {
      type: "Circle",
      coordinates: [46.83, 75.25],
      radius: 10000
    }
  };

  public options = {
    fillColor: '#00FF00',
    strokeColor: '#0000FF',
    opacity: 0.5,
    strokeWidth: 5,
    strokeStyle: 'shortdash'
  };


  ngOnInit(): void {
    this.touchedRows = [];
    this.userTable = this.fb.group({
      tableRows: this.fb.array([])
    });
    this.seedTable();
    console.log(this.points);
    // this.addRow();
  }

  seedTable() {
    const control =  this.userTable.get('tableRows') as FormArray;
    for (let i of this.rows) {
      control.push(this.fb.group(i));
    }
  }

  ngAfterOnInit() {
    this.control = this.userTable.get('tableRows') as FormArray;
  }

  initiateForm(): FormGroup {
    return this.fb.group({
      object_number: ['', Validators.required],
      vv_amount: ['', Validators.required],
      vagon_amount: ['', Validators.required],
      group_type: ['', Validators.required],
      group_name: ['', Validators.required],
      coord_x: [''],
      coord_y: [''],
      radius: ['', Validators.required],
      color: ['', Validators.required],
      isEditable: [true]
    });
  }

  addRow() {
    const control =  this.userTable.get('tableRows') as FormArray;
    for (let item of control.controls) {
      item.get('isEditable').setValue(false);
    }
    let group = this.initiateForm();
    control.push(group);
    this.currentGroup = group;
  }

  deleteRow(index: number) {
    const control =  this.userTable.get('tableRows') as FormArray;

    for (let i = 0; i < this.points.length; i++) {
        if (this.points[i].id == control.controls[index].get('object_number').value) {
          delete this.points[i];
          this._changeDetectorRef.detectChanges();
          break;
        }
    }
    control.removeAt(index);

  }

  editRow(group: FormGroup) {
    const control = this.userTable.get('tableRows') as FormArray;

    for (let item of control.controls) {
        item.get('isEditable').setValue(false);
    }
    this.currentGroup = group;
    group.get('isEditable').setValue(true);
  }

  doneRow(group: FormGroup) {
    group.get('isEditable').setValue(false);
  }

  saveUserDetails() {
    console.log(this.userTable.value);
  }

  get getFormControls() {
    const control = this.userTable.get('tableRows') as FormArray;
    return control;
  }

  object_keys(obj) {
    return Object.keys(obj);
  }

  set_group_names(id, group) {
    console.log(this.group_names);
    this.group_names[id] = Object.keys(this.rules[group.controls.group_type.value]);
  }

  set_radius(group) {
    group.get('radius').setValue(this.coef * Math.pow(group.controls.vv_amount.value, 0.333))
  }

  submitForm() {
    const control = this.userTable.get('tableRows') as FormArray;
    this.touchedRows = control.controls.filter(row => row.touched).map(row => row.value);
  }

  public onClick(event: IEvent): void {
    if (this.currentGroup && event.type === "dragend") {
      const coords = event.event.originalEvent.target.geometry._coordinates;
      this.currentGroup.get('coord_x').setValue(coords[0]);
      this.currentGroup.get('coord_y').setValue(coords[1]);
    }
    if (this.currentGroup && event.type === "click") {
      const coords = event.event.get('coords');
      this.currentGroup.get('coord_x').setValue(coords[0]);
      this.currentGroup.get('coord_y').setValue(coords[1]);
      this.set_radius(this.currentGroup);
      this.points.push(new Point(this.currentGroup.get('object_number'), coords[0], coords[1], this.currentGroup.get('radius').value*this.radius_coeff));
      this._changeDetectorRef.detectChanges();
    }
  }
}
