import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scheme',
  templateUrl: './scheme.component.html',
  styleUrls: ['./scheme.component.css']
})
export class SchemeComponent implements OnInit {
  numbers = [];
  constructor() {
    this.numbers = Array(100).fill(1);
  }

  ngOnInit(): void {
  }

}
